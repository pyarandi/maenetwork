import Foundation

public struct MAENetworkRequest {
    public init(url: URL, method: MAENetworkRequest.HTTPMethod) {
        self.url = url
        self.method = method
    }
    
    public enum HTTPMethod: String {
        case get
        case post
    }
    
    public var url: URL
    public var method: HTTPMethod
}

public protocol MAENetworkProtocol: AnyObject {
    func request<T: Decodable>(_ request: MAENetworkRequest, completion: @escaping (Result<T, Error>) -> Void)
}

public class MAENetwork {
    var session: URLSession
    
    public enum NetworkError: Error {
        case httpError(code: Int)
        case couldNotFoundData
    }
    
    public init(session: URLSession) {
        self.session = session
    }
}

extension MAENetwork: MAENetworkProtocol {
    public func request<T>(_ request: MAENetworkRequest, completion: @escaping (Result<T, Error>) -> Void) where T : Decodable {
        var urlRequest = URLRequest(url: request.url)
        urlRequest.httpMethod = request.method.rawValue.uppercased()
        session.dataTask(with: urlRequest) { data, response, error in
            
            if let error = error {
                completion(.failure(error))
                return
            }
            
            if let response = response as? HTTPURLResponse, response.statusCode != 200 {
                completion(.failure(NetworkError.httpError(code: response.statusCode)))
                return
            }
            
            if let data = data {
                do {
                    let decodedData = try JSONDecoder().decode(T.self, from: data)
                    completion(.success(decodedData))
                    return
                } catch {
                    completion(.failure(error))
                    return
                }
            }
            
            completion(.failure(NetworkError.couldNotFoundData))
        }
        .resume()
    }
}
