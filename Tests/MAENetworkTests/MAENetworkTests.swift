import XCTest
@testable import MAENetwork

final class MAENetworkTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(MAENetwork().text, "Hello, World!")
    }
}
